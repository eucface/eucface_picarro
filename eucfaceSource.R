setwd("c:/Data/FCP")

eucfaceLoggFileList<-list.files(path="C:/Data/FCP", pattern="LOGG4")


readEucFaceLoggFiles<- function(logfile=choose.files(), nlabelline=1){
  h <- readLines(logfile)[1:nlabelline]
  dat <- read.csv(logfile, skip= nlabelline, header=FALSE, strip.white=TRUE)
  colnames <- gsub("\"","",strsplit(paste(h, collapse=""), ",")[[1]])
  colnames <-gsub("[(,)]","",colnames)
  colnames <-gsub("^\\s+|\\s+$", "", colnames)
  names(dat) <- colnames
  return(dat)
}

testEucFaceLoggFile<-function(filename=basename(file.choose()),firstdate,lastdate){
  #   this function takes a filename cccYYMMDD.ccc and a startdate and enddate
  #   it returns the filename if the datestamp falls within the daterange otherwise returns null 
  year<-substring(filename,7,10)
  month<-substring(filename,11,12)
  day<-substring(filename,13,14)
  dated<-as.Date(paste(year,month,day,sep="-"))
  ##statement takes the files and returns the filename if the dated is within the range
  if (dated %in% firstdate:lastdate){
    return(filename)
  }
}

returnEucFaceLoggFile<-function(x,y,z){
  setwd("C:/Data/FCP")
  sd<-as.Date(x)
  ed<-as.Date(y)
  goodfiles<-lapply(z,testEucFaceLoggFile,sd,ed)
  ##the below sapply function skips the nulls in the goodfiles list
  logs <- lapply(goodfiles[!sapply(goodfiles, is.null)], readEucFaceLoggFiles)
  ##this line is now used outside the csv sub function
  ##goodfiles2<-do.call("rbind", logs)
  return(logs)
}

dataFrameofTestedEucFaceLoggFiles<-function(start, end, fileList,timeCuts){
  listOfEucFaceLoggFiles<-returnEucFaceLoggFile(as.character(as.Date(end)), 
                                                as.character(as.Date(start-7)), fileList)
  testedFileNameDataFrame<-rbindlist(listOfEucFaceLoggFiles)
  testedFileNameDataFrame$DateTime<-as.POSIXct(
    paste(testedFileNameDataFrame$DATE, 
          testedFileNameDataFrame$TIME, 
          sep= " ", tz="UTC"))
  testedFileNameDataFrame$DATE<-as.Date(testedFileNameDataFrame$DATE)
  testedFileNameDataFrame<-
    filter(testedFileNameDataFrame,
           DATE>=start&DATE<end)
  if(missing(timeCuts)) {
    return(testedFileNameDataFrame)
  }
  else{
    testedFileNameDataFrame$timeCuts<-cut(testedFileNameDataFrame$DateTime, timeCuts, right=TRUE, include.lowest = TRUE)
    return(testedFileNameDataFrame)
  }
}

plotWindspeed<-function(x){
  ggplot(x, aes(x=DateTime, y=WS)) + 
    geom_line()# + 
  #ylim(-5,40)
}


plotWinddir<-function(x){
  ggplot(x, aes(x=DateTime, y=WD)) + 
    geom_line()# + 
  #ylim(-5,40)
}

eucfaceDataFrame<-dataFrameofTestedEucFaceLoggFiles(start=Sys.Date()-1,
                                                    end=Sys.Date(),
                                                    eucfaceLoggFileList,
                                                    timeCuts="5 min")


cuts <- unique(as.numeric(eucfaceDataFrame$DateTime) %/% (86400/288)) * (86400/288)
cuts <- c(cuts, cuts[length(cuts)]+(86400/288)) 
cuts <- as.POSIXct(cuts, origin="1970-01-01")
eucfaceDataFrame$timeCuts <-  cut(eucfaceDataFrame$DateTime, cuts)

eucfaceDataFrameAverages<-
  group_by(eucfaceDataFrame,timeCuts) %>% 
  summarise_each(funs(mean))


eucfaceDataFrameAverages$DateTime<-as.POSIXct(eucfaceDataFrameAverages$timeCuts, 
                                              "%Y-%m-%d %H:%M:%S", 
                                              tz="UTC")
