Picarro Daily Report
========================================================


```
## Picarro Daily Report for 2016-02-23
```

```
## Created on: 2016-02-24 11:34:29
```



Wind Speed 
-------------------------
Ring 4

![plot of chunk Wind_Speed](C:/Data/picarro/2016-02-23/Wind_Speed-1.png)

Wind Direction
-------------------------
Ring 4

![plot of chunk Wind_Dir](C:/Data/picarro/2016-02-23/Wind_Dir-1.png)

HR_12CH4_dry
-------------------------
![plot of chunk 12CH4_dry](C:/Data/picarro/2016-02-23/12CH4_dry-1.png)

HR_Delta_iCH4_30s
-------------------------
![plot of chunk Delta_iCH4](C:/Data/picarro/2016-02-23/Delta_iCH4-1.png)

HP_Delta_iCH4_30s
-------------------------
![plot of chunk Delta_iCH4_P](C:/Data/picarro/2016-02-23/Delta_iCH4_P-1.png)

X12CO2_dry
-------------------------

```
## Warning: Removed 1 rows containing missing values (geom_point).
```

```
## Warning: Removed 1 rows containing missing values (geom_ribbon).
```

![plot of chunk X12CO2](C:/Data/picarro/2016-02-23/X12CO2-1.png)

Delta_Raw_iCO2
-------------------------

```
## Warning: Removed 1 rows containing missing values (geom_point).
```

```
## Warning: Removed 1 rows containing missing values (geom_ribbon).
```

![plot of chunk iCO2](C:/Data/picarro/2016-02-23/iCO2-1.png)

H2O
-------------------------

```
## Warning: Removed 1 rows containing missing values (geom_point).
```

```
## Warning: Removed 1 rows containing missing values (geom_ribbon).
```

![plot of chunk H2O](C:/Data/picarro/2016-02-23/H2O-1.png)

WindRose
-------------------------
![plot of chunk windRose](C:/Data/picarro/2016-02-23/windRose-1.png)


