REM This batsript looks inside of the Picarro Data folder its
REM subdirectories.  Copies the path of each file to a tempory
REM text file and performs an xcopy operation.  The xcopy operation
REM copies only files that are newer and overwrites.
REM It then delets the files
REM currently it copies files to C:\Data\picarro
dir "\\1644-CFIDS2074\DataLog_User\" /A /B /S > tempListOfDirs.txt
For /F "tokens=*" %%A IN (tempListOfDirs.txt) Do (
If Exist %%A* (
XCOPY "%%A\*.dat" "C:\Data\picarro" /D /K /Y /R
)
)
del tempListOfDirs.txt